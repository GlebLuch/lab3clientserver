package org.example;

import org.example.db.DataBase;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;

public class Server {

    public static void main(String[] args) throws SQLException, IOException {
        try (ServerSocket server = new ServerSocket(8080)) {
            System.out.println("Server started");
            String request = null;
            StringBuilder result = new StringBuilder();


            try (Socket socket = server.accept();
                 DataOutputStream writer = new DataOutputStream(socket.getOutputStream());
                 DataInputStream reader = new DataInputStream(socket.getInputStream())
            ) {
                var url = reader.readUTF();
                var password = reader.readUTF();
                var userName = reader.readUTF();
                DataBase dataBase = new DataBase(password, userName, url);
                dataBase.connect();
                System.out.println("Client connected to server");
                while (true) {
                    request = reader.readUTF();
                    if (request.equals("Client disconnected")) {
                        return;
                    }
                    System.out.println("Request: " + request);
                    var response = dataBase.doSql(request);
                    if (response == null) {
                        writer.writeUTF("null");
                    } else {
                        while (response.next()) {
                            int id = response.getInt("id");
                            String name = response.getString("name");
                            String author = response.getString("author");
                            result.append("ID: ")
                                    .append(id)
                                    .append("; ")
                                    .append("NAME: ")
                                    .append(name).append("; ")
                                    .append("AUTHOR: ")
                                    .append(author)
                                    .append("\n");
                        }
                        writer.writeUTF(result.toString());
                        result = new StringBuilder();

                        writer.flush();
                    }
                }
            }
        }
    }
}