package org.example.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

   // "jdbc:postgresql://localhost:5432/lab3";

    static String passwordKey;
    static String usernameKey;
    static String urlKey;

    public ConnectionManager(String passwordKey, String usernameKey, String urlKey) {
        this.passwordKey = passwordKey;
        this.usernameKey = usernameKey;
        this.urlKey = urlKey;
    }

    static {
        loadDriver();
    }

    private static void loadDriver() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    private ConnectionManager() {

    }

    public Connection open() {
        try {
            return DriverManager.getConnection(
                    urlKey,
                    usernameKey,
                    passwordKey);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
