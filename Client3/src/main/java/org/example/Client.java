package org.example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static final String INSERT = "INSERT into book(name, author) VALUES ('%s', '%s');";
    private static final String SELECT = "SELECT * FROM book;";


    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        Scanner scan2 = new Scanner(System.in);

        try (
                Socket socket = new Socket("127.0.0.1", 8080);
                DataOutputStream writer = new DataOutputStream(socket.getOutputStream());
                DataInputStream reader = new DataInputStream(socket.getInputStream())
        ) {
            System.out.println("Connected to server");

            Scanner scan3 = new Scanner(System.in);
            System.out.println("Введите адрес базы данных: ");
            String url = scan3.nextLine();
            writer.writeUTF(url);
            writer.flush();
            System.out.println("Введите пароль бд: ");
            String password = scan3.nextLine();
            writer.writeUTF(password);
            writer.flush();
            System.out.println("Введите имя пользователя: ");
            String userName = scan3.nextLine();
            writer.writeUTF(userName);
            writer.flush();

            var flag = true;
            while (flag) {
                System.out.println("Выберите действие: \n" +
                        "1 - создать запись в базе\n" +
                        "2 - получить список записей в базе\n" +
                        "3 - завершить работу");
                var step = scan.nextInt();
                switch (step) {
                    case (1): {
                        System.out.println("Введите название книги: ");
                        var name = scan2.nextLine();
                        System.out.println("Введите имя автора: ");
                        var author = scan2.nextLine();
                        String insert = String.format(INSERT, name, author);
                        System.out.println("Request: " + insert);
                        writer.writeUTF(insert);
                        writer.flush();
                        System.out.println("Response: " + reader.readUTF());
                        break;
                    }
                    case (2): {
                        writer.writeUTF(SELECT);
                        writer.flush();
                        System.out.println("Response:\n" + reader.readUTF());
                        break;
                    }
                    case (3): {
                        flag = false;
                        writer.writeUTF("Client disconnected");
                        writer.flush();
                        break;
                    }
                }
            }
        }
    }
}